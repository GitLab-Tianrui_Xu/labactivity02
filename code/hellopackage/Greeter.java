package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.*;
class Greeter {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter an integer");
        int user = input.nextInt();
        Utilities u = new Utilities();
        System.out.println(u.doubleMe(user));
    }

}